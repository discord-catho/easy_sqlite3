from dktotoolkit import write_message
def insert_data(self, table_name:str, data_dict:dict, allow_duplicates:bool=True, commit:bool=True, replace_on_datas=None, new_datas_col=None, reverse_replace_on_datas=False, verbose=False):
    """
    Insère des données dans une table.

    :param str table_name: Nom de la table cible.
    :param dict data_dict: Dictionnaire contenant les colonnes et leurs valeurs.
    :param bool allow_duplicates: Indique si les doublons sont autorisés (par défaut: True).
    :param bool commit: Indique si la transaction doit être validée immédiatement (par défaut: False).
    :param str,list replace_on_datas: Colonnes à conserver pour mise à jour avec de nouvelles valeurs si elles existent.
    :param bool reverse_replace_on_datas: si True: remplacer les colonnes nommées
    :return: None
    :rtype: None
    """

    columns = ", ".join(data_dict.keys())
    #placeholders = ":" + ", :".join(data_dict.keys()) #[e if e is not None else "NULL" for e in data_dict.values()])
    placeholders = ", ".join(["?" for e in data_dict.keys()]) #[e if e is not None else "NULL" for e in data_dict.values()])

    datas = list(data_dict.values())

    if replace_on_datas is not None:
        if reverse_replace_on_datas:
            old_datas={k:v for k,v in data_dict.items() if not k in replace_on_datas}
            new_datas={k:v for k,v in data_dict.items() if k in replace_on_datas}
        else:
            old_datas={k:v for k,v in data_dict.items() if k in replace_on_datas}
            new_datas={k:v for k,v in data_dict.items() if not k in replace_on_datas}
        #

        if verbose:
            print(f"INPUT: {data_dict}", "debug")
            print(f"old  : {old_datas}", "debug")
            print(f"new  : {new_datas}", "debug")
            print(  self.get_datas(table_name=table_name, conditions=old_datas, if_present=True, verbose=verbose) )
        #
    #



    # Mettez à jour les colonnes spécifiées dans replace_on_datas si elles existent
    if (replace_on_datas and
        self.get_datas(table_name=table_name, conditions=old_datas, if_present=True, verbose=verbose) ):

        query = f"UPDATE {table_name} SET  "

        # Nouvelles données
        qtmp,dtmp=self._dict_to_conditions(new_datas, where=False)
        query = query + qtmp
        datas = dtmp

        # Anciennes donnees qui doivent matcher
        qtmp,dtmp=self._dict_to_conditions(old_datas)
        query = query + qtmp
        datas.extend(dtmp)

        write_message(f"Update datas in table : {query} :: datas = {datas}")

    elif allow_duplicates:
        query = f"INSERT INTO {table_name} ({columns}) VALUES ({placeholders})"
        write_message("May insert duplicated datas in table")
    else:
        query = f"INSERT OR IGNORE INTO {table_name} ({columns}) VALUES ({placeholders})"
        # query = f"INSERT OR REPLACE INTO {table_name} ({columns}) VALUES ({placeholders})"
    #endIf

    try:
        print(query, datas)
        self.add_db(query=query, datas=datas, commit=commit)
    except Exception as e:
        print(e)
        raise
#endIl

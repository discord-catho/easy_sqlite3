from typing import Union
def get_datas(self, table_name:str, fields:Union[list, tuple]=None, conditions:dict={}, if_present:bool=False, verbose=False):
    """
    Récupère des données depuis une table.

    :param str table_name: Nom de la table.
    :param list or tuple fields: Liste des champs à récupérer (par défaut: None, ce qui signifie tous les champs).
    :param dict conditions: Conditions pour filtrer les résultats (par défaut: {}).
    :param bool if_present: Indique si on doit vérifier si au moins une ligne existe (par défaut: False).

    :return: Si `if_present` est True, retourne le nombre de lignes existantes (test possible, car if 0 = False)
             Si `if_present` est False, retourne les données.
    :rtype: bool or list
    """

    # Todo : ajouter table, columns, ...
    #
    if fields is None:
        fields = "*"
    elif isinstance(fields,(list, tuple)):
        fields = ",".join(fields)
    elif not isinstance(fields,str):
        raise ValueError(f"Type of fields: {type(fields)} ({fields})")
    #

    if if_present:
        query = f"SELECT COUNT(*) from {table_name} "
    else:
        query = f"SELECT {fields} from {table_name} "
    #
    datas=None

    if conditions:
        qtmp,datas = self._dict_to_conditions(conditions=conditions)
        query+=qtmp
    #

    if verbose:
        print(query, datas)
    #
    result = self.request_db(query=query, datas=datas)

    if verbose:
        print(int(result[0][0]) if if_present else result)
    #
    return int(result[0][0]) if if_present else result
#

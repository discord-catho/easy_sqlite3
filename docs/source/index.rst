.. easysqlite3 documentation master file, created by
   sphinx-quickstart on Sat May 27 14:46:46 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to easysqlite3's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   readme
   easysqlite3

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.

import os
import sys
sys.path.insert(0, os.path.abspath('..'))
sys.path.insert(0, os.path.abspath('../..'))

os.environ["PYTHONPATH"] = ".."

# -- Project information -----------------------------------------------------

project = 'easysqlite3'
copyright = '2023, Pierre'
author = 'Pierre'

# The full version, including alpha/beta/rc tags
release = '1.0.0'


# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
  'sphinx_rtd_theme',
  'sphinx.ext.autodoc',  # Autodoc
  'sphinx.ext.autosummary',  # Ne pas ecrire les autoX
  "myst_parser",    # Add the README
]

#spécifier le nom du répertoire où les pages de documentation seront générées
automodapi_toctreedirnm = 'api'

templates_path = ['_templates']
exclude_patterns = []

language = 'fr'

os.environ['SPHINX_BUILD'] = '1'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']

autodoc_default_options = {
    'members': True,
    'member-order': 'bysource',
    'special-members': '__init__',
    'undoc-members': True,
    'exclude-members': '__weakref__'
}
